from datetime import datetime, timedelta
import calendar
import requests
import re
from flask import Flask, render_template, request
import json
import sqlite3

from metar import Metar
from taf import Taf


app = Flask(__name__)
app.debug = True
app.secret_key = 'LKASJDLAKSJDLASJD'


def get_icao_code(name):
    conn = sqlite3.connect("//Users//evgenijkostogorov//PycharmProjects//untitled6//SQLite//test.db")
    cur = conn.cursor()

    cur.execute("""SELECT icao, rus_name FROM russian_airports
               WHERE rus_name LIKE :city
               OR eng_name LIKE :city
               OR icao LIKE :city
               OR iata LIKE :city""", {"city": "%" + name + "%"})
    result = cur.fetchall()

    return {"icao": result[0][0], "name": result[0][1]}



def get_metar(airport_code): #getting METAR info
    url_metar = "http://tgftp.nws.noaa.gov/data/observations/metar/stations/{0}.TXT".format(airport_code.upper())
    r_metar = requests.get(url_metar)

    if r_metar.ok:
        text_metar = r_metar.text
        return text_metar.strip()
    else:
        return "No such airport"



def get_taf(airport_code):
    url = "https://avwx.rest/api/taf/" + airport_code.upper()
    r = requests.get(url)
    if r.ok:
        json = r.json()
        if len(json) > 1:
            return json["Raw-Report"]
        else:
            return "No forecast data"
    else:
        return "No data"


def translate_month(month): # translator for russian dates
    months_dict = {"january": "января",
                   "february": "февраля",
                   "march": "марта",
                   "april": "апреля",
                   "may": "мая",
                   "june": "июня",
                   "july": "июля",
                   "august": "августа",
                   "septemper": "сентября",
                   "october": "октября",
                   "november": "ноября",
                   "december": "декабря"}

    if month.lower() in months_dict:
        return months_dict[month.lower()]
    else:
        return month


def metar_output(metar_object):
    date = metar_object.get_header()
    metar_object.analyze_metar()
    delay_risk = metar_object.count_delay_risk() # getting METAR risk of delay

    output = "Сейчас риск задержки вылета {0} (на основе метеоданных от {1})".format(delay_risk, date)
    return {"output" : output, "delay" : delay_risk}



def taf_output(taf_object):
    groups_output_text_list = []
    taf_object.get_weather_groups()
    parsed_groups = taf_object.parse_groups()  # getting parsed weather groups

    now = datetime.now()
    curr_month = now.strftime("%B")  # getting current month
    curr_day = now.strftime("%d")  # getting current day
    curr_month_days = calendar.monthrange(now.year, now.month)[1]

    for header, delay_risk in parsed_groups.items():
        start_date_str = re.findall(r"\d{4}\/", header)
        end_date_str = re.findall(r"/\d{4}", header)
        start_hour = start_date_str[0][-3: -1]
        start_day = start_date_str[0][0: 2]
        end_hour = end_date_str[0][-2:]
        end_day = end_date_str[0][1: 3]

        if curr_day == curr_month_days and end_day == "01":  # check if today is the last day of month
            curr_month = (datetime.now() + timedelta(days=30)).strftime("%B")  # replace curr month => next month
        else:
            pass

        group_output = "С {0}:00 {1} {2} по {3}:00 {4} {2} риск задержки вылета {5}".format(start_hour, start_day,
                                                                                           translate_month(curr_month),
                                                                                            end_hour, end_day, delay_risk)

        groups_output_text_list.append(group_output)

    return groups_output_text_list

# extra function for JSON outut of METAR + TAF
def taf_output_json(taf_object):
    returning_list = []
    taf_object.get_weather_groups()
    parsed_groups = taf_object.parse_groups()  # getting parsed weather groups

    now = datetime.now()
    curr_month = now.strftime("%m")  # getting current month
    curr_day = now.strftime("%d")  # getting current day
    curr_month_days = calendar.monthrange(now.year, now.month)[1]
    curr_year = now.strftime("%Y")

    count = 0
    for header, delay_risk in parsed_groups.items():
        count += 1
        start_date_str = re.findall(r"\d{4}\/", header)
        end_date_str = re.findall(r"/\d{4}", header)
        start_hour = start_date_str[0][-3: -1]
        start_day = start_date_str[0][0: 2]
        end_hour = end_date_str[0][-2:]
        end_day = end_date_str[0][1: 3]

        if curr_day == curr_month_days and end_day == "01":  # check if today is the last day of month
            curr_month = (datetime.now() + timedelta(days=30)).strftime("%B")  # replace curr month => next month
        else:
            pass

        group_output = {"id" : str(count), "start": curr_year + "-" + curr_month + "-" + start_day + "T" + start_hour +
                     ":00:00", "end": curr_year + "-" + curr_month + "-" + end_day + "T" +
                                      end_hour + ":00:00", "content": delay_risk, "className" : "green"}
        returning_list.append(group_output)

    return json.dumps(returning_list)



@app.route('/')
def home():
    return render_template('metar.html')

@app.route('/', methods=['POST', "GET"])
def delay():
    arp_name = request.form["airport-form"] # get html form value
    db_response = get_icao_code(arp_name.lower()) # get data from db
    icao_code = db_response["icao"] # get icao code
    airport = db_response["name"] # get airport name
    arp_string = "Погода в" + " " + airport.capitalize()

    metar_code = get_metar(icao_code) # get metar code
    if metar_code != "No such airport":
        try:
            metar_object = Metar(metar_code)
            metar_text = metar_output(metar_object)["output"]
            metar_data = metar_output(metar_object)["delay"]
        except:
            metar_text = "No current weather data"
            metar_data = "No data"
    else:
        metar_text = "No such airport"
        metar_data = "No data"


    taf_code = get_taf(icao_code) # get taf code
    if taf_code != "No such airport":
        try:
            taf_object = Taf(taf_code)
            taf_text = taf_output(taf_object)
            taf_json = taf_output_json(taf_object)
        except:
            taf_text = "No forecast data"
            taf_json = {"data" : "no_data"}
    else:
        taf_text = "No such airport"
        taf_json = {"data" : "no_airport"}


    return render_template('metar.html', metar = metar_text, taf = taf_text, airport = arp_string, json = taf_json, metar_delay = metar_data)


if __name__ == '__main__':
    app.run()